﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

	/// <summary>
	/// Version 1.2 30/03/2018
	/// This menus are the ones present on this screen, the 0th element will be the one visible at the start of the scene, all other elements will be centered, and made invisible
	/// </summary>
	public Image[] containedMenus;
	public bool debugMode;

	[Header("Sound Control")]
	public bool hasSound;
	public Sprite soundOn, soundOff;
	public Sprite musicOn, musicOff;

	public Image imageButton;

	public Button defaultButton;


	public void HideMenu(Image im){
		CanvasGroup cgm = im.GetComponent<CanvasGroup> ();
		if (cgm != null) {
			cgm.alpha = 0;
			cgm.blocksRaycasts = false;
			cgm.interactable = false;
		}
	}

	public void ShowMenu(Image im){
		if (im != null) {
			CanvasGroup cgm = im.GetComponent<CanvasGroup> ();
			if (cgm != null) {
				cgm.alpha = 1;
				cgm.blocksRaycasts = true;
				cgm.interactable = true;
			}
		}
	}

	public void InitializeMenu(Image menu){
		Vector2 pos = new Vector2 (0,0);
		if (menu != null) {
			menu.GetComponent<RectTransform> ().anchoredPosition = pos;
			HideMenu (menu);
		}
	}

	IEnumerator WaitFor(float timeToWait, string func){
		yield return new WaitForSeconds (timeToWait);
		this.SendMessage(func);
	}

	public void GotoScene(string levelName){
		SceneManager.LoadScene (levelName);
	}

	public void Start () {
		OrganizeMenus ();

		if (!PlayerPrefs.HasKey ("HasSound")) {
			PlayerPrefs.SetInt ("HasSound", 1);
			Debug.Log("Has Sound Key created");
			PlayerPrefs.Save ();
		}

		if(defaultButton != null){
			defaultButton.Select();
		}
		/*
		if (!PlayerPrefs.HasKey ("HasMusic")) {
			PlayerPrefs.SetInt("HasMusic",1);
			PlayerPrefs.Save();
		}
		*/

	}

	public void SoundCheck () {
		if (PlayerPrefs.GetInt ("HasSound") == 1) {
			hasSound = true;
			ActivateSound ();
		} else {
			hasSound = false;
			DeactivateSound ();
		}
	}

	public void AlternateSound(){
		hasSound = !hasSound;
		if (hasSound) {
			ActivateSound ();
			PlayerPrefs.SetInt ("HasSound", 1);
		} else {
			DeactivateSound ();
			PlayerPrefs.SetInt ("HasSound", 0);
		}
		PlayerPrefs.Save ();
	}

	void ActivateSound(){
		AudioListener.volume = 1;
		if (imageButton != null) {
			imageButton.GetComponent<Image> ().sprite = soundOff;
		}
	}

	void DeactivateSound(){
		AudioListener.volume = 0;
		if (imageButton != null) {
			imageButton.GetComponent<Image> ().sprite = soundOn;
		}
	}

	public void OrganizeMenus (){
		if (containedMenus.Length > 0) {
			for (int i = 0; i < containedMenus.Length; i++) {
				InitializeMenu (containedMenus [i]);
			}
			ShowMenu (containedMenus [0]);
		}
		//Debug.Log("Menus are now aligned");
	}

	public void QuitGame () {
		Application.Quit();
	}
}
