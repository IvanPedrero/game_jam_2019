﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class BarkBehaviour : MonoBehaviour {

	// Use this for initialization
	public List<GameObject> boxesList;
	AudioSource aus;
	public AudioClip[] barkClips;

	public float barkLenght = 0.3f;

	void Awake(){
		aus = GetComponent<AudioSource>();
	}

	void Start () {
		boxesList = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("mouse 1")) {
            CameraShaker.Instance.ShakeOnce(5f, 4f, 0.1f, 1f);
            DestroyBoxes();
            int randomBark = Random.Range(0,barkClips.Length);
            aus.PlayOneShot(barkClips[randomBark]);
            //Invoke("EndBark",barkLenght);
        }
	}

	void EndBark(){
		aus.Stop();
	}

	void DestroyBoxes(){
		Debug.Log("BARKK!!!!");
		for(int i = 0; i < boxesList.Count; i++){
			Destroy(boxesList[i]);
		}
	}

	void BoxDetected(GameObject newBox){
		boxesList.Add(newBox);
	}

	void BoxLost(GameObject oldBox){
		boxesList.RemoveAt(boxesList.IndexOf(oldBox));
	}
}
