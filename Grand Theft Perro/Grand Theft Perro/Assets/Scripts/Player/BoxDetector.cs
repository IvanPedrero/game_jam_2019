﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxDetector : MonoBehaviour {

	// Use this for initialization

	void OnTriggerEnter(Collider col){
		if(col.tag == "Box"){
			//boxesList.Add(col.transform.gameObject);
			transform.parent.SendMessage("BoxDetected",col.transform.gameObject);
		}
	}

	void OnTriggerExit(Collider col){
		if(col.tag == "Box"){
			//boxesList.Add(col.transform.gameObject);
			transform.parent.SendMessage("BoxLost",col.transform.gameObject);
		}
	}



}
