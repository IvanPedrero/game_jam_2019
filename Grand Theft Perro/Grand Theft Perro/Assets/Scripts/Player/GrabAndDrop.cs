﻿using System.Collections;
using UnityEngine;

public class GrabAndDrop : MonoBehaviour {

	// Use this for initialization
	public float grabRange;

	//asegurate de que no sea 0
	public float smooth = 1;

	public float minDistance = 3;
	public float maxDistante = 20;

	public bool modeKin = false;

	GameObject objectInHand;
	float grabbedObjectsize;
	public float objectDistance = 2;
	float objectSizeMultiplier = 1;
	GameObject mainCamera;
	GrabableObject ro;
	float inputFromTriggers = 0;

	LineRenderer line; 

	void Awake(){
		mainCamera = GameObject.FindWithTag("MainCamera");
	}

	void Start () {

	}
	
	void Update () {
		//Alternacion de modos;
		if( Input.GetKeyDown(KeyCode.Y)){
			modeKin = !modeKin;
		}

		//Debug.Log ("Smooth = " + smooth);
		//Dar Click
		if(Input.GetMouseButtonDown(0)){
			//Debug.Log(GetMouseHoverObject(grabRange));
			//Si no tengo objeto
			if(objectInHand == null){
				//tratar de agarrar el objeto que tengo en el mouse hover sobre la distancia adecuada
				TryGrabObject(GetMouseHoverObject(grabRange));
			}else{
				//si tengo objeto, tirarlo
				DropObject();
			}
		}


		if(objectInHand != null){
			
			//objectDistance = Mathf.Clamp(objectDistance,minDistance,maxDistante);


			Vector3 newPosition;
			if(modeKin){
				//This  one is stable but phases trough walls
				newPosition = gameObject.transform.position + Camera.main.transform.forward * objectDistance;
			}else{
				//This one is unstable, but does not phase
				objectInHand.GetComponent<Rigidbody>().isKinematic = false;
				newPosition = Vector3.Lerp(objectInHand.transform.position, mainCamera.transform.position + mainCamera.transform.forward * objectDistance, Time.deltaTime * smooth);
			}
			objectInHand.transform.position = newPosition;

		}else{

		}

	}


	//Grab the object only if it has a rigid body and a Resizable object script
	bool CanGrab(GameObject candidate){
		if((candidate.GetComponent<Rigidbody>() != null) && (candidate.GetComponent<GrabableObject>() != null)){
			return true;
		}else{
			return false;
		}
		//return candidate.GetComponent<Rigidbody>()  != null;
	}

	//Get the object the mouse is pointing to
	GameObject GetMouseHoverObject(float range){
		Vector3 position = gameObject.transform.position;
		RaycastHit raycastHit;
		Vector3 target = position + Camera.main.transform.forward * range;
		if(Physics.Linecast(position,target,out raycastHit)){
			return raycastHit.collider.gameObject;
		}
		return null;
	}

	void TryGrabObject(GameObject grabObject){
		if(grabObject != null && CanGrab(grabObject)){
			objectInHand = grabObject;
			ro = objectInHand.GetComponent<GrabableObject>();
			//Cambiar la distancia para que al darle click sea exactamente la distancia a la que esta
			//objectDistance = objectInHand.GetComponent<Renderer>().bounds.size.magnitude;
			objectDistance = Vector3.Distance(this.transform.position,objectInHand.transform.position);
			objectInHand.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
			objectInHand.GetComponent<Rigidbody>().useGravity = false;
			if(modeKin){
				objectInHand.GetComponent<Rigidbody> ().isKinematic = true;
			}

			//Parent the object to the camera, doesn't work to avoid collision
			//objectInHand.transform.parent = this.transform;
		
		}
	}

	void DropObject(){
		if(objectInHand != null){
			Rigidbody r = objectInHand.GetComponent<Rigidbody>();
			//Release all the object constrains
			r.constraints = RigidbodyConstraints.None;
			r.useGravity = true;
			r.velocity = new Vector3(0,0,0);
			if(modeKin){
				r.isKinematic = false;
			}
			//Way 2, not really needed
			//objectInHand.transform.parent = null;
			objectSizeMultiplier = 1;
			objectInHand = null;
		}
	}
}
