﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RouteBehaviour : MonoBehaviour {

	public bool drawRoute;
	public Color routeColor;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnDrawGizmos(){
		Gizmos.color = routeColor;
		if(drawRoute){
			for(int i = 0; i < transform.childCount - 1; i++){
				Gizmos.DrawLine(transform.GetChild(i).position, transform.GetChild(i+1).position);
			}
			Gizmos.DrawLine(transform.GetChild(transform.childCount-1).position, transform.GetChild(0).position);
		}

	}
}
