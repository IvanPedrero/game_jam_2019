﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class GameManager : MonoBehaviour {

    //Player health:
    [HideInInspector]
    public float totalHealth = 100f;
    [Range(0.0f, 100.0f)]
    public float health;

    //Player stamina:
    [HideInInspector]
    public float totalStamina = 100f;
    public float stamina;

    //Player eating variables:
    [HideInInspector]
    public float totalHunger;
    public float hunger;
    [HideInInspector]
    public float totalThirst;
    public float thirst;

    [Header("Speed variables : ")]
    public float normalSpeed = 2f;
    public float playerSpeedMultiplier;

    [Header("Status display :")]
    public Image healthBar;

    [Header("Player Rigidbody Controller :")]
    public UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController controller;

    // Use this for initialization
    void Start () {
        //Asign the variables.
        health = totalHealth;
        stamina = totalStamina;
        hunger = totalHunger;
        thirst = totalThirst;

        InitSpeeds();

        controller = GameObject.FindObjectOfType<RigidbodyFirstPersonController>();
    }
	
	// Update is called once per frame
	void Update () {
        //Get Luke's stamina back.
        RecoverStamina();
        //Set health bar.
        CalculateHealthDisplay();
        //Set the speed.
        SetPlayerSpeed();
	}

    //This method will display the total amount of blood shown in the canvas:
    void CalculateHealthDisplay()
    {
        EnableBloodAlpha(1-(health / totalHealth));
    }



    //This methods will set the speed of the player according to it's health:
    float speed_3_4, speed_1_2;
    void InitSpeeds()
    {
        playerSpeedMultiplier = normalSpeed;
        speed_3_4 = playerSpeedMultiplier * 0.85f;
        speed_1_2 = playerSpeedMultiplier * 0.65f;
    } 

    void SetPlayerSpeed()
    {
        
        if (health <= 70f)
        {
            playerSpeedMultiplier = speed_3_4;      //3/4.
        }

        if (health <= 40f)
        {
            playerSpeedMultiplier = speed_1_2;      //Half.
        }
        if (health <= 10f)
        {
            playerSpeedMultiplier = 1f;             //No more sprinting.
        }
        if(health > 70f)
        {
            playerSpeedMultiplier = normalSpeed;    //Normal sprinting.

        }
    }

    //This mehtod will display a given amount of alpha in the blood frame:
    void EnableBloodAlpha(float intensity)
    {
        var tempColor = healthBar.color;
        tempColor.a = intensity;
        healthBar.color = tempColor;
    }

    //This method will replenish stamina.
    void RecoverStamina()
    {
        //If the player stopped running...
        if (!controller.movementSettings.m_Running)
        {
            //If the stamina is not complete...
            if (stamina < totalStamina)
            {
                //Recover stamina.
                stamina++;
            }
        }
    }
}
